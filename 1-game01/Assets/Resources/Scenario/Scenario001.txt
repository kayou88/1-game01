fg

// 로딩
showloading
wait 0.1

loadbg town
loadbg Restaurant
loading Restaurant
loadbgm gg
loadmodel Haru
loadmodel Epsilon

bgm Cool
wait 0.1
removeloading
showmenu


text Lecture No.1\n- 01-나만의 미연시 스토리만들기 과제

bg bg1
fgout 1.0
wait 0.5

model Epsilon angry true empty (0,-1,0) 1.0


motion Epsilon angry
expression Epsilon

name 유라
text 일어나세요
motion Epsilon doNot

text 수업시작해요

text  저도 졸리지만 참고있어요.
motion Epsilon deny

text 하지만 눈을 떠야하죠..

motion Epsilon sad
text 이제 10주간 죽어라 공부해야되요
text 하면 할수록 더 어려워질거에요
text 신경 써야 하는 것들이 한두가지가 아니죠!

motion Epsilon idle


text 배고프고 어렵고 나중엔 VR까지 배워야 할 것들이 정말정말 많아요!

motion Epsilon happy

text 하지만 걱정 마세요! \n우리가 함께하는 100시간동안 열심히 배워도 제대로 할 수 있을지는 몰라요!

text 우선 제일 중요한 수강료는…
text 15억원이에요 >.<

motion Epsilon nod

text 돈도 실력인거 아시죠?

motion Epsilon nod

text 자 그럼 지갑에 얼마나 있는지 알아볼까요?

text 1. 커피쿠폰 \n2. 만원 \n3. 신용카드

label selectStart
expression Epsilon empty
motion Epsilon idle true
name
text 당신의 지갑속을 확인해 볼까요?

select
selectitem coupon 커피쿠폰
selectitem money 만원
selectitem card 신용카드
selectend

label coupon
name 나
text 천원커피 쿠폰밖에 없어요
text 음.. 생과일주스는 마시고 싶지만 아직은 도장이 부족해요 ㅠㅠ

expression Epsilon smile
name 유라
text 처음이니까 부족한 건 당연하죠! 하지만 걱정마세요!
expression Epsilon empty

text 다른사람들의 쿠폰을 합치면 무료음료를 마실 수 있어요-
text (중략)
text 그럼 이제 쿠폰을 가지러 가볼까요 ^^

jump selectStart

label money
name 나
text 만원이 있는데, 왜 유니티를 배워야 하나요?

expression Epsilon smile
name 유라
text 유니티를 배우는데는 그만한 이유가 있지요!
text 음, 이유를 나열하자면 대략 --가지가 있을거 같네요 :)
expression Epsilon empty

text (중략)
text 그럼 이제 만원의 행복말고 시원하게 꺼내볼까요? ㅎㅎ

jump selectStart

label card

name 나
text 유라! 저 바로 카드를 긁어보는 실습을 해보고 싶어요 ^^ 

expression Epsilon surprise
name 유라
text 오, 바로 해보고 싶으세요?!

expression Epsilon happy

text 저와 함께 즐거운 부자놀이를 해보아요^^
text 마침 저기에 오늘 우리를 도와줄 친구들이 있네요!

fg 1.0
wait 1.0

hidetext

bg bg3
bgm gg
fgout 1.0
wait 0.5

model Epsilon idle true empty (0,-1,0) 1.0


motion Epsilon surprise
expression Epsilon

name 유라
text 큰일이에요
motion Epsilon happy


text 뒤에 쟤들 보여요?
text 당신의 지갑을 들고 뛰어가고있네요 >.<

motion Epsilon happy
text 머리는 나빠도 달리기는 빠르시겠죠?

loadmodel guan
model guan idle true empty (0,-1,0) 1.0
